<?php
/**
 * @file
 * Contains the DraggableViews order-per-parent value handler.
 */

/**
 * Field handler to provide unique-per-parent values.
 */
class views_handler_field_draggableviews_order_per_parent extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function render($values) {
    if (strcmp($this->view->plugin_name, 'draggabletable')) {
      return '';
    }
    $base_field = $this->view->base_field;
    $style_options = $this->view->style_options;

    if (!isset($style_options['tabledrag_hierarchy']['field'])) {
      return '';
    }

    $info = $this->view->draggableviews_info;
    $prop = $info['nodes'][$values->{$base_field}];

    $order = array();
    $prev['depth'] = -1;
    foreach ($info['nodes'] AS $nid => $current) {
      if ($current['depth'] > $prev['depth']) {
        $order[$current['depth']] = 0;
      }
      else {
        $order[$current['depth']]++;
      }

      if ($nid == $values->{$base_field}) {
        break;
      }

      $prev = $current;
    }

    return $order[$prop['depth']];
  }
}
