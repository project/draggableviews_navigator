<?php
/**
 * @file
 * Contains the DraggableViews navigator.
 */

/**
 * Field handler to provide navigation links.
 */
class views_handler_field_draggableviews_navigator extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function render($values) {
    if (strcmp($this->view->plugin_name, 'draggabletable')) {
      return '';
    }
    return 'not yet implemented';
  }
}
